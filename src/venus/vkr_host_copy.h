/*
 * Copyright 2025 Google LLC
 * SPDX-License-Identifier: MIT
 */

#ifndef VKR_HOST_COPY_H
#define VKR_HOST_COPY_H

#include "vkr_common.h"

void
vkr_context_init_host_copy_dispatch(struct vkr_context *ctx);

#endif /* VKR_HOST_COPY_H */
